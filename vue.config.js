module.exports = {
  // indexPath: '../src/public/index.html',
  outputDir: 'public',
  publicPath: '',
  assetsDir: './',
  productionSourceMap: false,
  chainWebpack: (config) => {
    config
      .plugin('html')
      .tap((args) => {
        args[0].template = './src/public/index.html';
        return args;
      });
  },
};
